package Metro;

/**
 * A simple enumeration to represent lines in the Boston Metro System
 * 
 */

public enum Lines {

	RED, REDA, REDB, GREEN, GREENB, GREENC, GREEND, GREENE, BLUE, ORANGE, MATTAPAN;

}
