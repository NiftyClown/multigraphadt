package Metro;
import System.Edge;


public class MetroLine extends Edge<Station> {
	private Lines line;
	
	public MetroLine(Lines line)
	{
		this.line = line;
	}
	
	public void setLine(Lines line) {
		this.line = line;
	}
	
	public Lines getLine() {
		return line;
	}
}
