package Metro;
import java.util.ArrayList;
import java.util.List;

import System.Graph;
import System.Multigraph;

/** 
 * Representation of the Boston Metro
 * while will extend the Graph class
 * and implement the pathfinding.
 * 
 */

public class BostonMetro  {
	
	private Graph<Station, MetroLine> graph;
	
	/** Constructor for the class */
	public BostonMetro() {
		graph = new Multigraph<Station, MetroLine>(MetroLine.class);
	}
	
	public boolean addStation(Station station) {
		// add station to graph
		return graph.addNode(station);
	}
	
	public boolean addLine(Station st1, Station st2, Lines line) {
		// create an edge
		MetroLine ml = graph.addEdge(st1, st2);
		
		// check if edge is ok
		if (ml == null)	{
			// \TODO deal with better!!
			return false;
		}
		
		// set the line
		ml.setLine(line);
		return true;
	}
	
	public Station getStation(int station_id) {
		for (Station station : graph.getNodes()) {
			if (station.getID() == station_id) {
				return station;
			}
		}
		
		// \TODO deal with better!
		return null;
	}
	
	public List<Station> getDirections(int start_id, int end_id)
	{
		// \TODO implement
		return null;
	}
	
	/** Path find method */
//	public List<Station<E>> pathFind (int startID, int endID) {
//		
//	}
	
	
}

	
