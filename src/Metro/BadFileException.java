package Metro;

@SuppressWarnings("serial")
public class BadFileException extends Exception {

	public BadFileException(String message)
    {
       super(message);
    }

}
