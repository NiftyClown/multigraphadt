package Metro;

/**
 * A specified implementation for the Boston Metro system.
 * 
 * Implements the Node class to permit further abstaction.
 * 
 */

public class Station {

	private int stationID;
	private String name;

	/** The constructor for the class */
	public Station(String name, int id) {
		this.name = name;
		this.stationID = id;
	}

	/** Getters */
	public int getID() {
		return this.stationID;
	}

	public String getName() {
		return this.name;
	}
}
