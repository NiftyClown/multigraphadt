package System;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * A basic implementation of the Graph ADT interface.
 * 
 */

public class Multigraph<N, E extends Edge<N>> implements Graph<N, E> {

	private List<N> nodes;
	private List<E> edges;
	private EdgeFactory<N, E> edge_factory;

	/**
	 * The constructor of the class.
	 */
	public Multigraph(Class<? extends E> edge_class) {
		edge_factory = new EdgeFactory<N, E>(edge_class);
		nodes = new ArrayList<N>();
		edges = new ArrayList<E>();
	}

	@Override
	public boolean addNode(N n) {
		if (nodes.contains(n))
			return false;

		nodes.add(n);
		return true;
	}

	@Override
	public List<N> getNodes() {
		return nodes;
	}
	
	@Override
	public E addEdge(N n1, N n2) {
		// check nodes are on the graph
		if (!nodes.contains(n1) || !nodes.contains(n2))	{
			return null;
		}
		
		E edge = edge_factory.createEdge(n1, n2);
		edges.add(edge);
		return edge;
	}
	
	@Override
	public E getEdge(N n1, N n2) {
		// check nodes are on the graph
		if (!nodes.contains(n1) || !nodes.contains(n2))	{
			return null;
		}
		
		// check node is neighbour
		if (!neighbours(n1).contains(n2))
		{
			// \TODO deal with better!!
			return null;
		}
		
		// return the edge between nodes
		for (E edge : edges)
		{
			if (edge.getA() == n1 && edge.getB() == n2)
			{
				return edge;
			}
			
			else if (edge.getA() == n2 && edge.getB() == n1)
			{
				return edge;
			}
		}
		
		// \TODO deal with this better!
		return null;
	}
	
	@Override
	public HashSet<N> neighbours(N n) {
		HashSet<N> neighbours = new HashSet<N>();
		
		for (Edge<N> edge : edges)
		{
			if (edge.getA() == n)
			{
				neighbours.add(edge.getB());
			}
			
			else if (edge.getB() == n)
			{
				neighbours.add(edge.getA());
			}
		}
		
		return neighbours;
	}
}
