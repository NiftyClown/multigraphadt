package System;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * A basic respresentation of the graph being worked on
 * 
 */

public interface Graph<N, E extends Edge<N>> {

	/** Basic functions */
	public boolean addNode(N n);
	public E addEdge(N n1, N n2);
	HashSet<N> neighbours(N n);
	public List<N> getNodes();
	public E getEdge(N n1, N n2);
}
