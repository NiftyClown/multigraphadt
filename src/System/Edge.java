package System;

/**
 * A basic representation of an Edge class. Contains two elements: NodeA NodeB
 * 
 * 
 */

public class Edge<N> {

	N node_a, node_b;
	
	/* Methods */
	public Edge()
	{
		node_a = null;
		node_b = null;
	}
	
	public N getA()
	{
		return node_a;
		
	}
	
	public N getB()
	{
		return node_b;
		
	}
}
